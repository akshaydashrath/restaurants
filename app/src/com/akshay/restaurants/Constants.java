package com.akshay.restaurants;

public class Constants {
    
    public static final int THEME = com.akshay.restaurants.R.style.RestaurantsTheme;
    
    public static final int RADIUS = 1600;

}
