package com.akshay.restaurants.activities;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.Window;
import com.akshay.restaurants.BuildConfig;
import com.akshay.restaurants.R;
import com.akshay.restaurants.RestaurantsApplication;
import com.akshay.restaurants.activities.base.BaseFragNavigationActivity;
import com.akshay.restaurants.fragments.RestaurantsListFragment;
import com.akshay.restaurants.interfaces.ILocation;
import com.akshay.restaurants.util.Log;
import com.novoda.location.Locator;
import com.novoda.location.exception.NoProviderAvailable;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

public class RestaurantsActivity extends BaseFragNavigationActivity implements ILocation {

    private Locator locator;

    public final BroadcastReceiver freshLocationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateLocation();
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        setSupportProgressBarIndeterminateVisibility(false);
        locator = RestaurantsApplication.getLocator();
        if (savedInstanceState == null || shouldStartLocationUpdatesOnRotate()) {
            startLocationUpdates();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add("Refresh").setIcon(R.drawable.ic_refresh)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        startLocationUpdates();
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopLocationUpdates();
    }

    @Override
    public void startLocationUpdates() {
        IntentFilter f = new IntentFilter();
        f.addAction(RestaurantsApplication.LOCATION_UPDATE_ACTION);
        registerReceiver(freshLocationReceiver, f);
        try {
            locator.startLocationUpdates();
        } catch (NoProviderAvailable np) {
            Log.e("No available provider");
            Helper.showErrorDialog(this);
        }
    }

    @Override
    public void stopLocationUpdates() {
        try {
            unregisterReceiver(freshLocationReceiver);
            locator.stopLocationUpdates();
        } catch (Exception e) {
            Log.e("Making sure we are not unregistering location updates twice");
        }
    }

    @Override
    public boolean shouldStartLocationUpdatesOnRotate() {
        if (locator.getLocation().hasAccuracy() && locator.getLocation().getAccuracy() > 100) {
            return true;
        }
        return false;
    }

    @Override
    public void updateLocation() {
        RestaurantsListFragment list = (RestaurantsListFragment) getSupportFragmentManager().findFragmentById(
                R.id.places_list_fragment);
        double latitude = locator.getLocation().getLatitude();
        double longitude = locator.getLocation().getLongitude();
        list.setLocation(latitude, longitude);
        list.refreshData(this);
        if (BuildConfig.DEBUG) {
            Log.i("Latitiude = " + latitude + " Longitude = " + longitude);
        }
        stopLocationUpdatesBasedOnAccuracy();
    }

    @Override
    public void stopLocationUpdatesBasedOnAccuracy() {
        if (locator.getLocation().hasAccuracy()) {
            if (locator.getLocation().getAccuracy() < 100) {
                stopLocationUpdates();
            }
        } else {
            stopLocationUpdates();
        }
    }

    @Override
    protected int getListNavResource() {
        return R.array.list_nav;
    }

    public void fragmentFeedbackOnServiceCallStart() {
        setSupportProgressBarIndeterminateVisibility(true);
    }

    public void fragmentFeedbackOnServiceCallComplete(int resultCode) {
        setSupportProgressBarIndeterminateVisibility(false);
    }
}