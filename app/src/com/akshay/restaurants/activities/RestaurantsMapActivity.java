package com.akshay.restaurants.activities;

import java.util.List;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.Window;
import com.akshay.restaurants.BuildConfig;
import com.akshay.restaurants.Constants;
import com.akshay.restaurants.R;
import com.akshay.restaurants.RestaurantsApplication;
import com.akshay.restaurants.activities.base.BaseMapNavigationActivity;
import com.akshay.restaurants.builders.ApiIntentFactory;
import com.akshay.restaurants.interfaces.ILocation;
import com.akshay.restaurants.overlays.PlaceOverlay;
import com.akshay.restaurants.provider.queries.PlacesFactory;
import com.akshay.restaurants.provider.tables.Place;
import com.akshay.restaurants.service.DetachableResultReceiver;
import com.akshay.restaurants.util.Log;
import com.akshay.restaurants.util.TimeUtils;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;
import com.novoda.location.Locator;
import com.novoda.location.exception.NoProviderAvailable;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;

public class RestaurantsMapActivity extends BaseMapNavigationActivity implements DetachableResultReceiver.Receiver,
        ILocation {

    private static final String RETAINED_LONGITUDE = "retained_longitude";

    private static final String RETAINED_LATITUDE = "retained_latitude";

    private Locator locator;

    private DetachableResultReceiver receiver;

    private long initialTimeStamp;

    private MapView mapView;

    private static final String RETAINED_TIMESTAMP = "retained_timestamp";

    public final BroadcastReceiver freshLocationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateLocation();
        }
    };

    private List<Overlay> mapOverlays;

    private double latitude;

    private double longitude;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_simple);
        setSupportProgressBarIndeterminateVisibility(false);
        mapView = (MapView) findViewById(R.id.mapview);
        mapOverlays = mapView.getOverlays();
        init(savedInstanceState);
        mapView.setBuiltInZoomControls(true);
    }

    private void init(Bundle savedInstanceState) {
        setSupportProgressBarIndeterminateVisibility(false);
        locator = RestaurantsApplication.getLocator();
        receiver = new DetachableResultReceiver(new Handler());
        receiver.setReceiver(this);
        if (savedInstanceState == null) {
            startLocationUpdates();
            initialTimeStamp = System.currentTimeMillis();
            latitude = 51.5171;
            longitude = 0.1062;
        } else {
            initialTimeStamp = savedInstanceState.getLong(RETAINED_TIMESTAMP);
            latitude = savedInstanceState.getDouble(RETAINED_LATITUDE);
            longitude = savedInstanceState.getDouble(RETAINED_LONGITUDE);
            showPlacesOnMap(null);
            if (shouldStartLocationUpdatesOnRotate()){
                startLocationUpdates();
            }
        }

    }

    private Cursor loadCursor(Bundle bundle) {
        PlacesFactory factory = new PlacesFactory();
        long time = TimeUtils.getTimeStampForQuery(bundle, initialTimeStamp);
        return managedQuery(factory.uri(), factory.projections(), factory.selections(), factory.selectionArgs(time),
                factory.sortOrder());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add("Refresh").setIcon(R.drawable.ic_refresh)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        startLocationUpdates();
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putDouble(RETAINED_LATITUDE, latitude);
        outState.putDouble(RETAINED_LONGITUDE, longitude);
        outState.putLong(RETAINED_TIMESTAMP, initialTimeStamp);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopLocationUpdates();
    }

    @Override
    public void startLocationUpdates() {
        IntentFilter f = new IntentFilter();
        f.addAction(RestaurantsApplication.LOCATION_UPDATE_ACTION);
        registerReceiver(freshLocationReceiver, f);
        try {
            locator.startLocationUpdates();
        } catch (NoProviderAvailable np) {
            Log.e("No available provider");
            Helper.showErrorDialog(this);
        }
    }
    
    @Override
    public boolean shouldStartLocationUpdatesOnRotate() {
        if (locator.getLocation().hasAccuracy() && locator.getLocation().getAccuracy() > 100) {
            return true;
        }
        return false;
    }


    @Override
    public void stopLocationUpdates() {
        try {
            unregisterReceiver(freshLocationReceiver);
            locator.stopLocationUpdates();
        } catch (Exception e) {
            Log.e("Making sure we are not unregistering location updates twice");
        }
    }

    @Override
    public void updateLocation() {
        latitude = locator.getLocation().getLatitude();
        longitude = locator.getLocation().getLongitude();
        if (BuildConfig.DEBUG) {
            Log.i("Latitiude = " + latitude + " Longitude = " + longitude);
        }
        refreshData(latitude, longitude);
        stopLocationUpdatesBasedOnAccuracy();
    }

    @Override
    public void stopLocationUpdatesBasedOnAccuracy() {
        if (locator.getLocation().hasAccuracy()) {
            if (locator.getLocation().getAccuracy() < 100) {
                stopLocationUpdates();
            }
        } else {
            stopLocationUpdates();
        }
    }

    @Override
    protected int getListNavResource() {
        return R.array.map_nav;
    }

    protected boolean isRouteDisplayed() {
        return false;
    }

    public void refreshData(double latitude, double longitude) {
        setSupportProgressBarIndeterminateVisibility(true);
        startService(ApiIntentFactory.getPlacesNearMe(getApplicationContext(), latitude, longitude, Constants.RADIUS,
                receiver));

    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        showPlacesOnMap(resultData);
        setSupportProgressBarIndeterminateVisibility(false);
    }

    private void showPlacesOnMap(Bundle resultData) {
        mapOverlays.clear();
        Cursor cursor = loadCursor(resultData);
        Drawable drawable = getResources().getDrawable(R.drawable.ic_places);
        PlaceOverlay itemizedoverlay = new PlaceOverlay(drawable, this);
        while (cursor != null && cursor.moveToNext()) {
            double latitude = cursor.getDouble(cursor.getColumnIndex(Place.Columns.LAT));
            double longitude = cursor.getDouble(cursor.getColumnIndex(Place.Columns.LNG));
            String title = cursor.getString(cursor.getColumnIndex(Place.Columns.NAME));
            String snippet = cursor.getString(cursor.getColumnIndex(Place.Columns.VICINITY));
            GeoPoint point = new GeoPoint((int) (latitude * 1e6), (int) (longitude * 1e6));
            OverlayItem overlayitem = new OverlayItem(point, title, snippet);
            itemizedoverlay.addOverlay(overlayitem);
        }
        mapOverlays.add(0, itemizedoverlay);
        showMyLocationOnMap(latitude, longitude);
        mapView.invalidate();

    }

    private void showMyLocationOnMap(double lat, double lng) {
        Drawable drawable2 = this.getResources().getDrawable(R.drawable.ic_my_loc);
        PlaceOverlay itemizedoverlay2 = new PlaceOverlay(drawable2, this);
        String title = "My location";
        String snippet = "";
        GeoPoint point = new GeoPoint((int) (lat * 1e6), (int) (lng * 1e6));
        OverlayItem overlayitem2 = new OverlayItem(point, title, snippet);
        itemizedoverlay2.addOverlay(overlayitem2);
        mapOverlays.add(1, itemizedoverlay2);
        mapView.getController().animateTo(point);
        mapView.getController().setZoom(15);
    }

}