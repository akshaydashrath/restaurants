package com.akshay.restaurants.activities.base;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;

import com.actionbarsherlock.R;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.app.SherlockMapActivity;
import com.akshay.restaurants.builders.IntentFactory;

public abstract class BaseMapNavigationActivity extends SherlockMapActivity {
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayShowTitleEnabled(false);
        ab.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        ab.setListNavigationCallbacks(setupSpinnerAdapter(), new OnListNavListener());
    }

    private SpinnerAdapter setupSpinnerAdapter() {
        return ArrayAdapter.createFromResource(this, getListNavResource(),
                R.layout.sherlock_spinner_dropdown_item);
    }
    
    protected abstract int getListNavResource();

    private class OnListNavListener implements OnNavigationListener {

        private boolean synthetic = true;
        private String[] strings = getResources().getStringArray(getListNavResource());

        @Override
        public boolean onNavigationItemSelected(int position, long itemId) {

            if (synthetic) {
                synthetic = false;
                return true;
            }

            String selected = strings[position];
            if (selected.equals(getString(R.string.list_nav))) {
                startActivityFromList(IntentFactory.getPlaces());
            } else if (selected.equals(getString(R.string.map_nav))) {
                startActivityFromList(IntentFactory.getMap());
            } else {
                return false;
            }
            return true;
        }
    }

    private void startActivityFromList(Intent intent) {
        startActivity(intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
        finish();
    }

}
