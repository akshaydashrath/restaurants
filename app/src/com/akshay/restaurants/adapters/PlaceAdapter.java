package com.akshay.restaurants.adapters;

import com.akshay.restaurants.R;
import com.akshay.restaurants.RestaurantsApplication;
import com.akshay.restaurants.provider.tables.Place;
import com.akshay.restaurants.util.DistanceUtil;
import com.novoda.imageloader.core.model.ImageTag;
import com.novoda.imageloader.core.model.ImageTagFactory;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class PlaceAdapter extends CursorAdapter {

    private LayoutInflater inflater;
    private ImageTagFactory imageTagFactory;

    public PlaceAdapter(Context context, Cursor c) {
        super(context, c, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        inflater = LayoutInflater.from(context);
        imageTagFactory = new ImageTagFactory(100, 100, R.drawable.ic_launcher);
        imageTagFactory.setErrorImageId(R.drawable.ic_launcher);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        final ViewHolder holder = (ViewHolder) view.getTag();
        holder.placeName.setText(cursor.getString(cursor.getColumnIndex(Place.Columns.NAME)));
        double distance = cursor.getDouble(cursor.getColumnIndex(Place.Columns.DISTANCE));
        holder.distance.setText(DistanceUtil.getDistance(distance)+" " + DistanceUtil.getDistanceSclae(context, distance));
        holder.address.setText(cursor.getString(cursor.getColumnIndex(Place.Columns.VICINITY)));
        String url = cursor.getString(cursor.getColumnIndex(Place.Columns.ICON));
        ImageTag tag = imageTagFactory.build(url);
        holder.icon.setTag(tag);
        RestaurantsApplication.getImageManager().getLoader().load(holder.icon);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup group) {
        ViewHolder holder = new ViewHolder();
        View view = inflater.inflate(R.layout.list_item_places, null);
        holder.placeName = (TextView) view.findViewById(R.id.place_name);
        holder.distance = (TextView) view.findViewById(R.id.place_distance);
        holder.address = (TextView) view.findViewById(R.id.place_address);
        holder.icon = (ImageView) view.findViewById(R.id.place_icon);
        view.setTag(holder);
        return view;
    }
    
    @Override
    public boolean isEnabled(int position) {
        return false;
    }
    
    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    public static class ViewHolder {
        public ImageView icon;
        TextView placeName;
        TextView distance;
        TextView address;

    }

}
