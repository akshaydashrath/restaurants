package com.akshay.restaurants.provider.queries;

import android.net.Uri;

import com.akshay.restaurants.provider.tables.Place;

public class PlacesFactory implements QueryFactory {
    
    public Uri uri(){
        return Place.URI;
    }
    
    public String[] projections(){
        return null;
    }
    
    public String selections(){
        return  Place.Columns.LAST_MODIFIED
                + ">=?";
    }
    
    public String[] selectionArgs(long time){
        return new String[] { String.valueOf(time) };
    }
    
    public String sortOrder(){
        return Place.Columns.DISTANCE;
    }
    
   
}
