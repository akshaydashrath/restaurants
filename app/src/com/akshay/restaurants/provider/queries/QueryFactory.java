package com.akshay.restaurants.provider.queries;

import android.net.Uri;

public interface QueryFactory {
    Uri uri();
    String[] projections();
    String selections();
    String[] selectionArgs(long time);
    String sortOrder();    
}
