package com.akshay.restaurants.provider;

import java.util.ArrayList;

import org.codehaus.jackson.JsonNode;

import com.akshay.restaurants.builders.HttpRequestBuilder;
import com.akshay.restaurants.provider.JsonToUVPair.UriToValuePair;


public class JsonMarshaller {

    public void insertTimestamp(HttpRequestBuilder builder, ProviderOperations operations){
     
    }
    

    public void marshall(JsonNode node, HttpRequestBuilder builder, ProviderOperations operations) throws Exception{
        ArrayList<UriToValuePair> objs = JsonToUVPair.getListOfDBObj(builder, node);
        for (UriToValuePair obj : objs) {
                operations.newInsert(obj.uri).withValues(obj.values).build();
        }
    }
    
    public void postMarshall(HttpRequestBuilder builder){
        
    }
}
