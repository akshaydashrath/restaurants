package com.akshay.restaurants.provider;

import java.util.ArrayList;

import org.codehaus.jackson.JsonNode;

import com.akshay.restaurants.BuildConfig;
import com.akshay.restaurants.builders.ApiIntentFactory;
import com.akshay.restaurants.builders.HttpRequestBuilder;
import com.akshay.restaurants.provider.tables.Place;
import com.akshay.restaurants.util.Log;

import android.content.ContentValues;
import android.location.Location;
import android.net.Uri;
import android.text.TextUtils;


public class JsonToUVPair {
    
    public static ArrayList<UriToValuePair> getListOfDBObj(HttpRequestBuilder builder, JsonNode node) throws Exception {
        switch (Constants.uriMatcher.match(UrlToUriMatcher.getUri(builder.getUrl()))) {
        case Constants.UriCodes.PLACES:
            String location = builder.getParam(ApiIntentFactory.Params.LOCATION);
            if (BuildConfig.DEBUG){
                Log.d("Location = " + location);
            }
            String[] coods = location.split(",");
            return getPlace(node, Double.parseDouble(coods[0]), Double.parseDouble(coods[1]));
        default:
            throw new Exception("No values found");
        }
    }

    private static ArrayList<UriToValuePair> getPlace(JsonNode node, double currentLat, double currentLon) throws Exception {
        if (node.isArray()) {
            throw new Exception("Arrays are not supported");
        }   
        double latitude = Double.parseDouble(getTextFromNode(node.get("geometry"), "location", "lat"));
        double longitude = Double.parseDouble(getTextFromNode(node.get("geometry"), "location", "lng"));    
        ArrayList<UriToValuePair> objs = new ArrayList<UriToValuePair>();
        UriToValuePair restaurants = new UriToValuePair();
        restaurants.uri = Place.URI;       
        ContentValues values = new ContentValues();
        values.put(Place.Columns._RID, getTextFromNode(node, null, "id"));
        values.put(Place.Columns.ICON, getTextFromNode(node, null, "icon"));
        values.put(Place.Columns.RATING, getTextFromNode(node, null, "rating"));
        values.put(Place.Columns.NAME, getTextFromNode(node, null, "name"));
        values.put(Place.Columns.LAT, latitude);
        values.put(Place.Columns.LNG, longitude);
        values.put(Place.Columns.REFERENCE, getTextFromNode(node, null, "reference"));
        values.put(Place.Columns.TYPES, getTextFromNode(node, null, "reference"));
        values.put(Place.Columns.VICINITY, getTextFromNode(node, null, "vicinity"));
        values.put(Place.Columns.LAST_MODIFIED, System.currentTimeMillis());
        float[] results = new float[3];
        Location.distanceBetween(currentLat, currentLon, latitude, longitude, results);
        if (results.length > 0)
            values.put(Place.Columns.DISTANCE, results[0]);
        restaurants.values = values;
        objs.add(restaurants);
        return objs;
    }

    private static String getTextFromNode(JsonNode node, String column, String path) {
        String text = " ";
        try {
            if (column != null && column.length() > 0) {
                text = node.get(column).findValue(path).asText();
            } else {
                text = node.findValue(path).asText();
            }
        } catch (Exception e) {
            return text;
        }
        if (TextUtils.isEmpty(text) || text.equals("null")) {
            return text;
        }
        return text;
    }


    public static class UriToValuePair {
        public ContentValues values;
        public Uri uri;
    }

}
