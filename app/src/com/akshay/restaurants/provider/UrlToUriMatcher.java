package com.akshay.restaurants.provider;

import java.util.HashMap;

import android.net.Uri;

public class UrlToUriMatcher {

    public static final int NO_MATCH = -1;

    private static final HashMap<String, Uri> map = new HashMap<String, Uri>();

    public static void addUrl(String url, Uri uri) {
        map.put(url, uri);
    }

    public static Uri getUri(String url) {
        Uri uri = Uri.parse(url);
        uri = map.get(url);
        return uri;
    }

}
