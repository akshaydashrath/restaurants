package com.akshay.restaurants.provider;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentValues;
import android.net.Uri;
import android.text.TextUtils;

public class ProviderOperations {

    private ArrayList<ContentProviderOperation> operations = new ArrayList<ContentProviderOperation>();

    public ArrayList<ContentProviderOperation> getContentProviderOperations() {
        return operations;
    }

    private Builder currentOperation;
    
    private String authority;

    public ProviderOperations(String authority){
        this.authority = authority;
    }

    public ProviderOperations() {
        this.authority = RestaurantsProvider.AUTHORITY;
    }

    public ProviderOperations newInsert(Uri uri) {
        currentOperation = ContentProviderOperation.newInsert(uri);
        return this;
    }

    public ProviderOperations newUpdate(Uri uri){
        currentOperation = ContentProviderOperation.newUpdate(uri);
        return this;
    }

    public ProviderOperations withValue(String key, Object value) {
        if (!TextUtils.isEmpty(String.valueOf(value)))
            currentOperation.withValue(key, value);
        return this;
    }
    public ProviderOperations withValues(ContentValues values) {
        if (values!=null)
            currentOperation.withValues(values);
        return this;
    }

    public ProviderOperations withSelection(String selection, String[] selectionArgs){
        currentOperation.withSelection(selection, selectionArgs);
        return this;
    }

    public ProviderOperations withValueBackReference(String columnName, int i) {
        currentOperation.withValueBackReference(columnName, i);
        return this;
    }

    public ProviderOperations withYieldAllowed(boolean isAllowed) {
        currentOperation.withYieldAllowed(isAllowed);
        return this;
    }

    public void build() {
        operations.add(currentOperation.build());
    }

    public int size() {
        return operations.size();
    }

    public List<ContentProviderOperation> getContentProviderOperationsSubList(int from, int to) {
        return operations.subList(from, to);
    }
    public void setAuthority(String authority){
        this.authority = authority;
    }

    public String getAuthority(){
        return this.authority;
    }

    public ProviderOperations newDelete(Uri uri) {
        currentOperation = ContentProviderOperation.newDelete(uri);
        return this;
    }

    public void clearContentProviderOperations() {
         operations = new ArrayList<ContentProviderOperation>();
    }


}