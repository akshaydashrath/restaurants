package com.akshay.restaurants.provider;

import com.akshay.restaurants.builders.ApiUrlFactory;
import com.akshay.restaurants.provider.tables.Place;

import android.content.UriMatcher;

public class Constants {
    
   
    public interface UriCodes{
        public static final int PLACES = 1;
        public static final int PLACE = 2;
    }
   
    public interface MimeTypes{
    	public static final String PLACES = "vnd.android.cursor.dir/vnd.com.akshay.restaurants.place";
    	public static final String PLACE = "vnd.android.cursor.item/vnd.com.akshay.restaurants.place";
    }
    
    
    public static UriMatcher uriMatcher;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(RestaurantsProvider.AUTHORITY, "place/", UriCodes.PLACES);
        uriMatcher.addURI(RestaurantsProvider.AUTHORITY, "place/#", UriCodes.PLACE);
    }
    
    
    static {
        UrlToUriMatcher.addUrl(ApiUrlFactory.getSearchUrl(), Place.URI);
    }

}
