package com.akshay.restaurants.provider;

import java.util.Iterator;

import org.codehaus.jackson.JsonNode;

import android.content.ContentProviderResult;
import android.content.Context;

import com.akshay.restaurants.RestaurantsApplication;
import com.akshay.restaurants.builders.HttpRequestBuilder;

public class JsonResponseHandler {

    private Context context;

    public JsonResponseHandler() {
        context = RestaurantsApplication.getContext();
    }

    public ContentProviderResult[] handleResponse(HttpRequestBuilder builder, byte[] response) {
        JsonMarshaller marshaller = new JsonMarshaller();
        ContentProviderResult[] result = null;
        try {
            final JsonNode rootNode = RestaurantsApplication.getJsonMapper().readValue(new String(response), JsonNode.class);
            ProviderOperations operations = new ProviderOperations();
            Iterator<JsonNode> iterator = rootNode.get("results").iterator();
            while (iterator.hasNext()) {
                iterateJson(builder, marshaller, operations, iterator);
            }
            result = context.getContentResolver().applyBatch(operations.getAuthority(),
                    operations.getContentProviderOperations());
            marshaller.postMarshall(builder);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private void iterateJson(HttpRequestBuilder builder, JsonMarshaller marshaller, ProviderOperations operations,
            Iterator<JsonNode> iterator) throws Exception {
        JsonNode node = iterator.next();
        if (node.isArray()) {
            Iterator<JsonNode> iterator2 = node.iterator();
            while (iterator2.hasNext()) {
                marshaller.marshall(iterator2.next(), builder, operations);
            }
        } else {
            marshaller.marshall(node, builder, operations);
        }
    }

}