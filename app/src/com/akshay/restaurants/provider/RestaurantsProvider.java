package com.akshay.restaurants.provider;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import novoda.lib.sqliteprovider.provider.SQLiteContentProviderImpl;

public class RestaurantsProvider extends SQLiteContentProviderImpl {

	public static final String AUTHORITY = "com.akshay.restaurants";
	
	/**
	 * @see android.content.ContentProvider#onCreate()
	 */
	@Override
	public boolean onCreate() {
		return super.onCreate();
	}
	

	/**
	 * @see android.content.ContentProvider#delete(Uri,String,String[])
	 */
	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		return super.delete(uri, selection, selectionArgs);
	}

	/**
	 * @see android.content.ContentProvider#insert(Uri,ContentValues)
	 */
	@Override
	public Uri insert(Uri uri, ContentValues initialValues) {
		return super.insert(uri, initialValues);
	}

	/**
	 * @see android.content.ContentProvider#query(Uri,String[],String,String[],String)
	 */
	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		return super.query(uri, projection, selection, selectionArgs, sortOrder);
	}

	/**
	 * @see android.content.ContentProvider#update(Uri,ContentValues,String,String[])
	 */
	@Override
	public int update(Uri uri, ContentValues initialValues, String selection,
			String[] selectionArgs) {
		return super.update(uri, initialValues, selection, selectionArgs);
	}

	@Override
	public String getType(Uri uri) {
		switch (Constants.uriMatcher.match(uri)) {
		case Constants.UriCodes.PLACE:
			return Constants.MimeTypes.PLACE;
		case Constants.UriCodes.PLACES:
			return Constants.MimeTypes.PLACES;
		default:
			throw new IllegalStateException("uri not known: " + uri);
		}
	}

}
