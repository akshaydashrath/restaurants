package com.akshay.restaurants.provider.tables;

import android.net.Uri;
import android.provider.BaseColumns;

import com.akshay.restaurants.provider.RestaurantsProvider;

public class Timestamp {
    
public static final Uri URI = Uri.parse("content://"+RestaurantsProvider.AUTHORITY+"/timestamp");
    
    public interface Columns extends BaseColumns{
        public static final String URL = "url";
        public static final String PARAMS = "params";
        public static final String LAST_MODIFIED_ON = "lastModifiedOn";
    }

}
