package com.akshay.restaurants.provider.tables;

import android.net.Uri;
import android.provider.BaseColumns;

import com.akshay.restaurants.provider.RestaurantsProvider;

public class Events {
    
public static final Uri URI = Uri.parse("content://"+RestaurantsProvider.AUTHORITY+"/events");
    
    public interface Columns extends BaseColumns {
        public static final String _RID = "_rid";
        public static final String SUMMARY = "summary";
        public static final String URL = "url";
        public static final String PLACE_ID = "place_id";
        public static final String LAST_MODIFIED = "last_modified";
    }

}
