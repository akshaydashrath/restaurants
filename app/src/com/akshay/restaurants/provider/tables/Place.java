package com.akshay.restaurants.provider.tables;

import com.akshay.restaurants.provider.RestaurantsProvider;

import android.net.Uri;
import android.provider.BaseColumns;

public class Place {
    
    public static final Uri URI = Uri.parse("content://"+RestaurantsProvider.AUTHORITY+"/place");
    
    public interface Columns extends BaseColumns {
        public static final String _RID = "_rid";
        public static final String ICON = "icon";
        public static final String NAME = "name";
        public static final String RATING = "rating";
        public static final String REFERENCE = "reference";
        public static final String LAST_MODIFIED = "last_modified";
        public static final String TYPES = "types";
        public static final String VICINITY = "vicinity";
        public static final String LAT = "lat";
        public static final String LNG = "lng";
        public static final String DISTANCE = "distance";
    }
}
