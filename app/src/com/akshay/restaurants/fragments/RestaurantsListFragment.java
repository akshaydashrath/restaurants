package com.akshay.restaurants.fragments;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockListFragment;
import com.akshay.restaurants.Constants;
import com.akshay.restaurants.activities.RestaurantsActivity;
import com.akshay.restaurants.adapters.PlaceAdapter;
import com.akshay.restaurants.builders.ApiIntentFactory;
import com.akshay.restaurants.provider.queries.PlacesFactory;
import com.akshay.restaurants.service.DetachableResultReceiver;
import com.akshay.restaurants.util.Log;
import com.akshay.restaurants.util.TimeUtils;

public class RestaurantsListFragment extends SherlockListFragment implements LoaderCallbacks<Cursor>,
        DetachableResultReceiver.Receiver {

    private static final String RETAINED_LIST_POSITION = "retained_list_position";
    private static final String RETAINED_TIMESTAMP = "retained_timestamp";
    private static final String RETAINED_LNG = "retained_lng";
    private static final String RETAINED_LAT = "retained_lat";
    private static final int PLACES_LOADER = 0;
    private DetachableResultReceiver receiver;
    private double latitude;
    private double longitude;
    private long initialTimeStamp;
    private int firstVisiblePosition;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        receiver = new DetachableResultReceiver(new Handler());
        receiver.setReceiver(this);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState == null) {
            latitude = 51.5171;
            longitude = 0.1062;
            initialTimeStamp = System.currentTimeMillis();
            refreshData(getActivity());
        } else {
            firstVisiblePosition = savedInstanceState.getInt(RETAINED_LIST_POSITION);
            latitude = savedInstanceState.getDouble(RETAINED_LAT);
            longitude = savedInstanceState.getDouble(RETAINED_LNG);
            initialTimeStamp = savedInstanceState.getLong(RETAINED_TIMESTAMP);
        }
        invokeLoader(null);
        setEmptyText("No restaurants nearby!");
    }

    @Override
    public void onResume() {
        super.onResume();
        getListView().setSelection(firstVisiblePosition);
    }

    @Override
    public void onPause() {
        super.onResume();
        firstVisiblePosition = getListView().getFirstVisiblePosition();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putDouble(RETAINED_LAT, latitude);
        outState.putDouble(RETAINED_LNG, longitude);
        outState.putLong(RETAINED_TIMESTAMP, initialTimeStamp);
        try {
            firstVisiblePosition = getListView().getFirstVisiblePosition();
            outState.putInt(RETAINED_LIST_POSITION, firstVisiblePosition);
        } catch (IllegalStateException e) {
            Log.e("Content view not created yet");
        }
    }

    protected void invokeLoader(Bundle resultData) {
        if (getActivity()==null){
            return;
        }
        getLoaderManager().initLoader(PLACES_LOADER, resultData, this);
    }

    protected void restartLoader(Bundle resultData) {
        if (getActivity()==null){
            return;
        }
        getLoaderManager().restartLoader(PLACES_LOADER, resultData, this);
    }

    @Override
    public void onListItemClick(ListView list, View view, int position, long id) {
    }

    @Override
    public Loader<Cursor> onCreateLoader(int loader, Bundle bundle) {
        switch (loader) {
        case PLACES_LOADER:
            PlacesFactory factory = new PlacesFactory();
            long time = TimeUtils.getTimeStampForQuery(bundle, initialTimeStamp);
            return new CursorLoader(getActivity().getApplicationContext(), factory.uri(), factory.projections(),
                    factory.selections(), factory.selectionArgs(time), factory.sortOrder());
        default:
            return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (getListAdapter() == null && getActivity() != null) {
            setListAdapter(new PlaceAdapter(getActivity(), cursor));
        } else {
            ((PlaceAdapter) getListAdapter()).changeCursor(cursor);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        loader = null;
        setListAdapter(null);
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        restartLoader(resultData);
        sendFeedbackToActivity(resultCode);
    }

    public void setLocation(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public void refreshData(Activity activity) {
        if (activity != null) {
            try {
                ((RestaurantsActivity) getActivity()).fragmentFeedbackOnServiceCallStart();
                activity.getApplicationContext()
                .startService(
                        ApiIntentFactory.getPlacesNearMe(activity.getApplicationContext(), latitude, longitude, Constants.RADIUS,
                                receiver));
            } catch (Exception e) {
                Log.e("Fragment must be invoked from restraunt activity to get feedback");
            }
        }
        
    }
    
    private void sendFeedbackToActivity(int resultCode) {
        if (getActivity() != null) {
            try {
                RestaurantsActivity activity = (RestaurantsActivity) getActivity();
                activity.fragmentFeedbackOnServiceCallComplete(resultCode);
            } catch (Exception e) {
                Log.e("Fragment must be invoked from restraunt activity to get feedback");
            }
        }
    }

}
