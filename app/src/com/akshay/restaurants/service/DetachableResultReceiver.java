package com.akshay.restaurants.service;


import com.akshay.restaurants.util.Log;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class DetachableResultReceiver extends ResultReceiver {

    private Receiver receiver;

    public DetachableResultReceiver(Handler handler) {
        super(handler);
    }

    public void clearReceiver() {
        receiver = null;
    }

    public void setReceiver(Receiver receiver) {
        this.receiver = receiver;
    }

    public interface Receiver {
        public void onReceiveResult(int resultCode, Bundle resultData);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if (receiver != null) {
            receiver.onReceiveResult(resultCode, resultData);
        } else {
            Log.i("Dropping result on floor for code " + resultCode);
        }
    }
}