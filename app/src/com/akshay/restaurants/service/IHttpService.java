package com.akshay.restaurants.service;

import com.akshay.restaurants.builders.HttpRequestBuilder;


public interface IHttpService {
    void processHttpRequest(HttpRequestBuilder requestBuilder);
}
