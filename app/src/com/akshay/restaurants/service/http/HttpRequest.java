package com.akshay.restaurants.service.http;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.zip.GZIPInputStream;

import org.apache.http.Header;
import org.apache.http.HttpResponse;

import com.akshay.restaurants.BuildConfig;
import com.akshay.restaurants.builders.HttpRequestBuilder;
import com.akshay.restaurants.service.http.client.CustomAndroidHttpClient;
import com.akshay.restaurants.util.Log;

import android.os.Bundle;

public abstract class HttpRequest {

    protected static final String HTTP_POST = "POST";
    protected static final String HTTP_GET = "GET";

    public static final String SERVICE_RESPONSE = "service_response";
    public static final String SERVICE_RESPONSE_CODE = "service_response_code";

    protected static CustomAndroidHttpClient httpclient;

    protected HttpRequestBuilder builder;

    public HttpRequest(HttpRequestBuilder builder) {
        this.builder = builder;
        if (httpclient == null) {
            try {
                httpclient = CustomAndroidHttpClient.newInstance("Songkick-Android");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    protected InputStream getInputStream(HttpResponse response) throws IOException {
        Header encodingHeader = response.getFirstHeader("Content-Encoding");
        InputStream in;
        if (encodingHeader != null) {
            logData("----------------------Content Encoding-------------- " + encodingHeader.getValue());
            if (encodingHeader.getValue().toLowerCase().equals("gzip")) {
                in = new GZIPInputStream(response.getEntity().getContent());
            } else {
                in = response.getEntity().getContent();
            }
        } else {
            in = response.getEntity().getContent();
        }
        return in;
    }

    protected void logData(String log) {
        if (BuildConfig.DEBUG) {
            Log.d(log);
        }
    }

    protected Bundle getBundleFromStream(int resultCode, byte[] content) throws IOException {
        Bundle bundle = new Bundle();
        if (content != null) {
            bundle.putByteArray(SERVICE_RESPONSE, content);
        }
        bundle.putInt(SERVICE_RESPONSE_CODE, resultCode);
        return bundle;
    }

    public abstract Bundle execute();

    protected abstract String getURL() throws MalformedURLException;

    public static class Settings {
        public static int CONN_TIMEOUT = 10000;
        public static int READ_TIMEOUT = 30000;
        public static int STALE_RESPONSE_DISPLAY_PERIOD = 5000;
        public static boolean DEBUG_MODE = false;
    }

}
