package com.akshay.restaurants.service.http;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;

import com.akshay.restaurants.builders.HttpRequestBuilder;
import com.akshay.restaurants.provider.JsonResponseHandler;
import com.akshay.restaurants.service.http.client.CustomAndroidHttpClient;

import android.os.Bundle;
import android.text.TextUtils;


public class HttpGetRequest extends HttpRequest {


    private HttpGet request;

    public HttpGetRequest(HttpRequestBuilder builder) {
        super(builder);
        request = new HttpGet(getURL());
        CustomAndroidHttpClient.modifyRequestToAcceptGzipResponse(request);
    }

    @Override
    public Bundle execute() {
        int resultCode = HttpStatusCodes.GATEWAY_TIMEOUT;
        boolean shouldCache = builder.isSavedToDB();
        Bundle resultData = new Bundle();
        logData("----------------------URL-------------- " + getURL());
        logData("----------------------Params-------------- " + builder.getParams());
        InputStream in = null;
        try {
            HttpResponse response = httpclient.execute(request);           
            resultCode = response.getStatusLine().getStatusCode();
            logData("----------------------Result Code-------------- " + resultCode);
            in = getInputStream(response);
            byte[] content = IOUtils.toByteArray(in);
            logData("----------------------Content-------------- " + new String(content));
            if (shouldCache) {
                new JsonResponseHandler().handleResponse(builder, content);
                resultData = getBundleFromStream(resultCode, null);
            } else {
                resultData = getBundleFromStream(resultCode, content);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return resultData;
    }


    @Override
    protected String getURL() {
        String param = builder.getParams();
        String sUrl = builder.getUrl();
        sUrl = (TextUtils.isEmpty(param)) ? sUrl : sUrl + "?" + param;
        return sUrl;
    }

}
