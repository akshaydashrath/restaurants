package com.akshay.restaurants.service.http;

import com.akshay.restaurants.builders.HttpRequestBuilder;
import com.akshay.restaurants.util.Log;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.ResultReceiver;

public class HttpRequestResponse {

    public static final int SERVICE_TYPE_GET = 0;
    public static final int SERVICE_TYPE_POST = 1;

    public interface NetworkAvailabilityCallback {
        public void isNetworkAvailable(boolean flag);
    }

    public Bundle doServiceCall(Context context, final HttpRequestBuilder serviceIntentBuilder) {
        final int serviceType = serviceIntentBuilder.getServiceType();
        ResultReceiver receiver = serviceIntentBuilder.getReceiver();
        Bundle bundle = new Bundle();
        if (isNetworkAvailable(context)) {
            switch (serviceType) {
            case SERVICE_TYPE_GET:
                bundle = new HttpGetRequest(serviceIntentBuilder).execute();
                break;
            case SERVICE_TYPE_POST:
                bundle = new HttpPostRequest(serviceIntentBuilder).execute();
                break;
            default:
                break;
            }
        } else {
            bundle.putInt(HttpRequest.SERVICE_RESPONSE_CODE, HttpStatusCodes.REQUEST_TIMEOUT);
        }
        if (receiver != null) {
            log(bundle);
            receiver.send(bundle.getInt(HttpRequest.SERVICE_RESPONSE_CODE), bundle);
        } else {
            Log.d("Receiver is null");
        }
        receiver = null;
        return bundle;
    }

    private void log(Bundle bundle) {
        Log.d("Sending response to receiver" + bundle.toString());
    }

    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return (activeNetworkInfo != null);
    }

}
