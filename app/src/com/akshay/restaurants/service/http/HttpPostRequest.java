package com.akshay.restaurants.service.http;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;

import com.akshay.restaurants.builders.HttpRequestBuilder;

import android.net.http.AndroidHttpClient;
import android.os.Bundle;


public class HttpPostRequest extends HttpRequest {

    private HttpPost request;

    public HttpPostRequest(HttpRequestBuilder builder) {
        super(builder);
        request = new HttpPost(getURL());
        AndroidHttpClient.modifyRequestToAcceptGzipResponse(request);
    }

    @Override
    public Bundle execute() {
        int resultCode = HttpStatusCodes.GATEWAY_TIMEOUT;
        Bundle resultData = new Bundle();
        logData("----------------------URL-------------- " + getURL());
        logData("----------------------Params-------------- " + builder.getParams());
        InputStream in = null;
        try {
            StringEntity entity = new StringEntity(builder.getParams());
            entity.setContentType(builder.getContentType());
            request.setEntity(entity);
            HttpResponse response = httpclient.execute(request);
            resultCode = response.getStatusLine().getStatusCode();
            logData("----------------------Result Code-------------- " + resultCode);
            in = getInputStream(response);
            byte[] content = IOUtils.toByteArray(in);
            logData("----------------------Content-------------- " + new String(content));
            resultData = getBundleFromStream(resultCode, content);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return resultData;
    }
    

    @Override
    protected String getURL() {
        return builder.getUrl();
    }

}
