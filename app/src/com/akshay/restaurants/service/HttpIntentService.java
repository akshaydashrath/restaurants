package com.akshay.restaurants.service;

import com.akshay.restaurants.builders.HttpRequestBuilder;
import com.akshay.restaurants.service.http.HttpRequestResponse;


import android.app.IntentService;
import android.content.Intent;

public class HttpIntentService extends IntentService implements IHttpService{

    public static final String TAG = "HttpIntentService";

    private HttpRequestResponse service;

    public HttpIntentService() {
        super(TAG);
        service = new HttpRequestResponse();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        HttpRequestBuilder requestBuilder = new HttpRequestBuilder(intent);
        processHttpRequest(requestBuilder);
    }

    public void processHttpRequest(HttpRequestBuilder requestBuilder) {
        service.doServiceCall(this, requestBuilder);
    }
}
