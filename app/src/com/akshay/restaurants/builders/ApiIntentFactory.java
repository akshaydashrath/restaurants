package com.akshay.restaurants.builders;

import com.akshay.restaurants.RestaurantsApplication;
import com.akshay.restaurants.service.DetachableResultReceiver;
import com.akshay.restaurants.service.http.HttpRequestResponse;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.ResultReceiver;


public class ApiIntentFactory {

    public static class Params {
        public static final String LOCATION = "location";
        public static final String RADIUS = "radius";
        public static final String TYPES = "types";
        public static final String NAME = "name";
        public static final String SENSOR = "sensor";
        public static final String KEY = "key";
    }

    public static class ContentTypes {
        public static final String APPLICATION_JSON = "application/json";
        public static final String APPLICATION_FORM_URL_ENCODED = "application/x-www-form-urlencoded";
        public static final String GZIP = "gzip";
    }   

    public static Intent getPlacesNearMe(Context context, double lat, double lng, int radius,
            final DetachableResultReceiver receiver) {
        HttpRequestBuilder builder = new HttpRequestBuilder(context);
        builder.setHttpType(HttpRequestResponse.SERVICE_TYPE_GET);
        addReceiverToRequest(receiver, builder);
        builder.setBaseUrl(Uri.parse(ApiUrlFactory.getSearchUrl()));
        builder.withParam(Params.KEY, RestaurantsApplication.getAPIKey());
        builder.withParam(Params.LOCATION, lat+","+lng);
        builder.withParam(Params.RADIUS, String.valueOf(radius));
        builder.withParam(Params.TYPES, "restaurant|bar");
        builder.withParam(Params.SENSOR, "true");
        builder.saveToDB(true);
        return builder.build();
    }
    
    private static void addReceiverToRequest(final ResultReceiver receiver, HttpRequestBuilder builder) {
        if (receiver != null) {
            builder.setResultReceiver(receiver);
        }
    }


}
