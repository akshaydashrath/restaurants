package com.akshay.restaurants.builders;


public class ApiUrlFactory {
    
    private static final String API_ENDPOINT = "place/";

    public static String getUrl() {
        return "https://maps.googleapis.com/";
    }

    public static String getApi() {
        return getUrl() + "maps/api/";
    }

    public static String getApiUrl() {
        return getApi() + API_ENDPOINT;
    }
    
    public static String getSearchUrl() {
        return getApiUrl() + "search/json";
    }
}