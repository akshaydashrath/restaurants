package com.akshay.restaurants.builders;

import com.akshay.restaurants.provider.tables.Place;

import android.content.Intent;
import static android.content.Intent.ACTION_VIEW;


public class IntentFactory {

    public static final String ACTION_VIEW_MAP = "android.intent.action.VIEW_MAP";
    
    public static Intent getPlaces(){
        return new Intent(ACTION_VIEW, Place.URI);
    }
    
    public static Intent getMap(){
        return new Intent(ACTION_VIEW_MAP, Place.URI);
    }

}
