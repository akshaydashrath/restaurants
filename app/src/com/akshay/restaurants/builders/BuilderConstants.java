package com.akshay.restaurants.builders;

public class BuilderConstants {
    
    public static final String INTENT_EXTRA_RESULT_RECEIVER = "intent_extra_result_receiver";
    public static final String SYNC_INTENT_EXTRA_POST_CONTENT = "sync_intent_extra_post_content";
    public static final String SYNC_INTENT_EXTRA_CONTENT_TYPE = "content_type";
    public static final String SYNC_INTENT_EXTRA_RECEIVER = "sync_intent_extra_receiver";
    public static final String SYNC_INTENT_EXTRA_SERVICE_TYPE = "sync_intent_extrac_service_type";
    public static final String SYNC_INTENT_EXTRA_PARAM = "sync_intent_extra_param";
    public static final String SYNC_INTENT_EXTRA_SHOULD_CACHE = "sync_intent_extra_should_cache";

}
