package com.akshay.restaurants.builders;

import java.util.List;

import com.akshay.restaurants.service.HttpIntentService;


import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.ResultReceiver;
import android.text.TextUtils;

import static com.akshay.restaurants.builders.BuilderConstants.*;

public class HttpRequestBuilder {

    private final Intent intent;

    public HttpRequestBuilder(Intent intent) {
        this.intent = intent;
    }

    public HttpRequestBuilder(Context context) {
        this.intent = new Intent(context, HttpIntentService.class);
    }
    
    public HttpRequestBuilder(Context context, Class<? extends IntentService> serviceClass) {
        this.intent = new Intent(context, serviceClass);
    }

    public HttpRequestBuilder setHttpType(int type) {
        intent.putExtra(SYNC_INTENT_EXTRA_SERVICE_TYPE, type);
        return this;
    }

    
    public HttpRequestBuilder saveToDB(boolean flag) {
        intent.putExtra(SYNC_INTENT_EXTRA_SHOULD_CACHE, flag);
        return this;
    }

    public HttpRequestBuilder setResultReceiver(ResultReceiver receiver) {
        intent.putExtra(SYNC_INTENT_EXTRA_RECEIVER, receiver);
        return this;
    }

    public HttpRequestBuilder withParam(String key, String value) {
        intent.setData(intent.getData().buildUpon().appendQueryParameter(key, value).build());
        return  this;
    }

    public HttpRequestBuilder setContentType(String contentType) {
        intent.putExtra(SYNC_INTENT_EXTRA_CONTENT_TYPE, contentType);
        return this;
    }

    public HttpRequestBuilder setBaseUrl(Uri uri) {
        intent.setData(uri);
        return this;
    }
    
    public String getParam(String param){
        Uri uri = intent.getData();
        return uri.getQueryParameter(param);
    }

    public String getParams() {
        Uri uri = intent.getData();
        return uri.getEncodedQuery();
    }

    public String getContentType() {
        String contentType = intent.getStringExtra(SYNC_INTENT_EXTRA_CONTENT_TYPE);
        if (TextUtils.isEmpty(contentType)) {
            return "";
        }
        return contentType;
    }

    public ResultReceiver getReceiver() {
        return intent.getParcelableExtra(SYNC_INTENT_EXTRA_RECEIVER);
    }

    public int getServiceType() {
        return intent.getIntExtra(SYNC_INTENT_EXTRA_SERVICE_TYPE, 0);
    }

    public String getUrl() {
        Uri uri = intent.getData();
        String url = uri.getScheme() + "://" + uri.getAuthority();
        List<String> pathList = uri.getPathSegments();
        for (String path : pathList) {
            url = url + "/" + path;
        }
        return url;
    }

    public boolean isSavedToDB() {
        return intent.getBooleanExtra(SYNC_INTENT_EXTRA_SHOULD_CACHE, false);
    }

    public Intent build() {
        return this.intent;
    }

}
