package com.akshay.restaurants.util;

import android.os.Bundle;

import com.akshay.restaurants.service.http.HttpRequest;
import com.akshay.restaurants.service.http.HttpStatusCodes;

public class TimeUtils {

    public static long getTimeStampForQuery(Bundle bundle, long initialTimeStamp) {
        if (bundle != null && bundle.getInt(HttpRequest.SERVICE_RESPONSE_CODE) == HttpStatusCodes.OK) {
            return initialTimeStamp;
        }
        return 0;
    }
}
