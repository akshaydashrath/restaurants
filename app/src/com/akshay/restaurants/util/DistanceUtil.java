package com.akshay.restaurants.util;

import java.math.BigDecimal;

import com.akshay.restaurants.R;

import android.content.Context;

public class DistanceUtil {
    
    private static final double MILE_METER_FACTOR = 1609.344;

    public static String getDistance(double distanceInMts){
        if (distanceInMts <= 1000){
            BigDecimal bd = getTruncatedDecimal(distanceInMts);
            return String.valueOf(bd);
        } else {
            double distanceInMiles = distanceInMts/MILE_METER_FACTOR;
            BigDecimal bd = getTruncatedDecimal(distanceInMiles);
            return String.valueOf(bd);
        }
    }
    
    public static String getDistanceSclae(Context context, double distanceInMts){
        if (distanceInMts <= 1000){
            return context.getString(R.string.distance_measurement_units_mtrs);
        } else {
            return context.getString(R.string.distance_measurement_units_miles);
        }
    }

    private static BigDecimal getTruncatedDecimal(double distanceInMts) {
        BigDecimal bd = new BigDecimal(distanceInMts);
        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
        return bd;
    }

}
