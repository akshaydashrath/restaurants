package com.akshay.restaurants.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public class StrictMode {

    private static final String BUILD = "build";
    private static final String PENALTY_LOG = "penaltyLog";
    private static final String DETECT_ALL = "detectAll";
    private static final String SET_THREAD_POLICY = "setThreadPolicy";
    private static final String THREAD_POLICY = "android.os.StrictMode$ThreadPolicy";
    private static final String STRICT_MODE = "android.os.StrictMode";
    private static final String BUILDER = "android.os.StrictMode$ThreadPolicy$Builder";

    public void enable() {
        try {
            Class<?> strictModeClass = Class.forName(STRICT_MODE, true, Thread.currentThread().getContextClassLoader());
            Class<?> threadPolicyClass = Class.forName(THREAD_POLICY, true, Thread.currentThread()
                    .getContextClassLoader());
            Class<?> threadPolicyBuilderClass = Class.forName(BUILDER, true, Thread.currentThread()
                    .getContextClassLoader());
            Method setThreadPolicyMethod = strictModeClass.getMethod(SET_THREAD_POLICY, threadPolicyClass);
            Method detectAllMethod = threadPolicyBuilderClass.getMethod(DETECT_ALL);
            Method penaltyMethod = threadPolicyBuilderClass.getMethod(PENALTY_LOG);
            Method buildMethod = threadPolicyBuilderClass.getMethod(BUILD);
            Constructor<?> threadPolicyBuilderConstructor = threadPolicyBuilderClass.getConstructor();
            Object threadPolicyBuilderObject = threadPolicyBuilderConstructor.newInstance();
            Object obj = detectAllMethod.invoke(threadPolicyBuilderObject);
            obj = penaltyMethod.invoke(obj);
            Object threadPolicyObject = buildMethod.invoke(obj);
            setThreadPolicyMethod.invoke(strictModeClass, threadPolicyObject);
        } catch (Exception ex) {
            Log.e("Strict mode not available.");
        }
    }

}
