package com.akshay.restaurants.interfaces;

public interface ILocation {
    void startLocationUpdates();
    void stopLocationUpdates();
    void updateLocation();
    void stopLocationUpdatesBasedOnAccuracy();
    boolean shouldStartLocationUpdatesOnRotate();
}
