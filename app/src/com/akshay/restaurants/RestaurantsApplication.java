package com.akshay.restaurants;

import org.codehaus.jackson.map.ObjectMapper;

import com.akshay.restaurants.util.StrictMode;
import com.novoda.imageloader.core.ImageManager;
import com.novoda.imageloader.core.LoaderSettings;
import com.novoda.imageloader.core.cache.LruBitmapCache;
import com.novoda.location.Locator;
import com.novoda.location.LocatorFactory;
import com.novoda.location.LocatorSettings;

import android.app.Application;
import android.content.Context;

public class RestaurantsApplication extends Application {
    
    private static RestaurantsApplication instance;
    private static String key;
    private static ObjectMapper mapper;
    private static Locator locator;
    private static ImageManager imageManager;
    
    public static final String PACKAGE_NAME = "com.akshay.restaurants";
    public static final String LOCATION_UPDATE_ACTION = "com.akshay.restaurants.ACTION_FRESH_LOCATION";

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        key = "AIzaSyCDBZxPMPv51nVEPjpKhTZ7xJsR5g8kBGA";
        enableStrictModeIfInDebug();
        initLocationManager();
        initImageManager();
    }
    
    private void initImageManager() {
        LoaderSettings settings = new LoaderSettings.SettingsBuilder().withDisconnectOnEveryCall(true)
                .withCacheManager(new LruBitmapCache(this, 50)).build(this);
        imageManager = new ImageManager(this, settings);
    }
    
    private void initLocationManager() {
        LocatorSettings settings = new LocatorSettings(PACKAGE_NAME, LOCATION_UPDATE_ACTION);
        settings.setUpdatesDistance(500);
        settings.setUseGps(true);
        locator = LocatorFactory.getInstance();
        locator.prepare(getApplicationContext(), settings);
    }

    private void enableStrictModeIfInDebug() {
        if (BuildConfig.DEBUG){
            new StrictMode().enable();
        }
    }
    
    public static ImageManager getImageManager(){
        return imageManager;
    }
    
    public static Context getContext(){
        return instance;
    }
    
    public static final Locator getLocator() {
        return locator;
    }
    
    public static String getAPIKey(){
        return key;
    }
    
    public static ObjectMapper getJsonMapper() {
        if (mapper == null) {
            mapper = new ObjectMapper();
        }
        return mapper;
    }

}
