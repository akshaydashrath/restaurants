package com.akshay.restaurants.test;

import com.akshay.restaurants.util.DistanceUtil;

import android.test.AndroidTestCase;

public class TestDistanceUtils extends AndroidTestCase {
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    public void testGetDistanceFor900mts(){
        double distance = 900.9999999;
        String dstStr = DistanceUtil.getDistance(distance);
        assert(dstStr.equals("900.99"));
    }
    
    public void testGetDistanceFor1100mts(){
        double distance = 1100.9999999;
        String dstStr = DistanceUtil.getDistance(distance);
        assert(dstStr.equals("1.10"));
    }
    
    public void testGetDistanceScaleForMtr(){
        double distance = 900.9999999;
        String scale = DistanceUtil.getDistanceSclae(getContext(), distance);
        assert(scale.equals(getContext().getString(com.akshay.restaurants.R.string.distance_measurement_units_mtrs)));
    }
    
    public void testGetDistanceScaleForMiles(){
        double distance = 1100.9999999;
        String scale = DistanceUtil.getDistanceSclae(getContext(), distance);
        assert(scale.equals(getContext().getString(com.akshay.restaurants.R.string.distance_measurement_units_miles)));
    }

}
